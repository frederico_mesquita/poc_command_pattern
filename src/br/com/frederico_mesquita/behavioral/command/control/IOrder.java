package br.com.frederico_mesquita.behavioral.command.control;

public interface IOrder {
	void execute();
}
