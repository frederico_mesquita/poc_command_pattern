package br.com.frederico_mesquita.behavioral.command.control;

import br.com.frederico_mesquita.behavioral.command.entity.Pizza;

public class Cancel implements IOrder {
	
	private static Cancel _instance = null;
	
	private Pizza pizza = null;
	
	private Cancel() {}
	
	private Cancel(Pizza pPizza) {
		this.pizza = pPizza;
	}
	
	public static Cancel getInstance(Pizza pPizza) {
		_instance = new Cancel(pPizza);
		return _instance;
	}

	@Override
	public void execute() {
		pizza.cancelOrder();
	}

}
