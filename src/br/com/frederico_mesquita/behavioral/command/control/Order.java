package br.com.frederico_mesquita.behavioral.command.control;

import br.com.frederico_mesquita.behavioral.command.entity.Pizza;

public class Order implements IOrder{
	
	private static Order _instance = null;
	
	private Pizza pizza = null;
	
	private Order() {}
	
	private Order(Pizza pPizza) {
		this.pizza = pPizza;
	}
	
	public static Order getInstance(Pizza pPizza) {
		_instance = new Order(pPizza);
		return _instance;
	}

	@Override
	public void execute() {
		pizza.makeOrder();
	}

}
