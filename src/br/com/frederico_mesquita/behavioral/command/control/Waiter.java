package br.com.frederico_mesquita.behavioral.command.control;

import java.util.ArrayList;
import java.util.List;

public class Waiter {
	private static Waiter _instance = null;
	
	private List<IOrder> lstiOrder = new ArrayList<IOrder>();
	
	private Waiter() {}
	
	public static Waiter getInstance() {
		if(null == _instance) {
			_instance = new Waiter();
		}
		return _instance;
	}
	
	public void takeOrder(IOrder pIOrder) {
		this.lstiOrder.add(pIOrder);
	}
	
	public void placeOrder() {
		for(IOrder item : this.lstiOrder) {
			item.execute();
		}
	}
}
