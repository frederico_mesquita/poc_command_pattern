package br.com.frederico_mesquita.behavioral.command;

import br.com.frederico_mesquita.behavioral.command.control.Cancel;
import br.com.frederico_mesquita.behavioral.command.control.Order;
import br.com.frederico_mesquita.behavioral.command.control.Waiter;
import br.com.frederico_mesquita.behavioral.command.entity.Pizza;
import br.com.frederico_mesquita.behavioral.command.entity.Sabor;
import br.com.frederico_mesquita.behavioral.command.entity.Size;

public class Customer {

	public static void main(String[] args) {		
		Waiter waiter = Waiter.getInstance();
		waiter.takeOrder(Order.getInstance(Pizza.getInstance(Sabor.CALABRESA, Size.GIANT, 3)));
		waiter.takeOrder(Order.getInstance(Pizza.getInstance(Sabor.PORTUGUESA, Size.SMALL, 5)));
		waiter.takeOrder(Cancel.getInstance(Pizza.getInstance(Sabor.PORTUGUESA, Size.MEDIUM, 2)));
		
		waiter.placeOrder();
	}

}
