package br.com.frederico_mesquita.behavioral.command.entity;

public enum Sabor {
	CALABRESA, PEPERONI, QUATRO_QUEIJOS, PORTUGUESA, NAPOLITANA;
}
