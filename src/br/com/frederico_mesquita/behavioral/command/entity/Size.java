package br.com.frederico_mesquita.behavioral.command.entity;

public enum Size {
	SMALL, MEDIUM, BIG, GIANT;
}
